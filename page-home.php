<?php
/**
 * Template Name: Home
 */
get_header(); ?>
    <div class="welcome-section d-flex align-items-center py-5">
        <div class="welcome-section__img">
            <img src="<?php bloginfo('template_url'); ?>/assets/img/welcome-img.jpg" alt="Evento Promnite">
        </div>
        <div class="welcome-section__pattern" style="background-image: url(<?php bloginfo('template_url'); ?>/assets/img/pattern-1.svg);"></div>
        <div class="container text-center position-relative py-5">
            <h1 class="text-uppercase text-center text-gold mb-4">Promnite</h1>
            <button>
                <img src="<?php bloginfo('template_url'); ?>/assets/img/play-btn.svg" alt="Play button">
            </button>
        </div>
    </div>

    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 mx-lg-auto">
                    <h2 class="text-uppercase h6 mb-4">Manifesto</h2>
                    <div class="h3 font-weight-light">
                        <?php the_field('manifesto'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="py-5 quote-section">
        <img src="<?php bloginfo('template_url'); ?>/assets/img/pattern-2.svg" alt="pattern" class="pattern-one">
        <div class="container py-5">
            <div class="row align-items-center">
                <div class="col-lg-5">
                    <p class="h2 font-weight-bold font-italic">
                        <?php echo get_field('frase_de_valor')['texto']; ?>
                    </p>
                </div>
                <div class="col-lg-7 position-relative">
                    <img src="<?php bloginfo('template_url'); ?>/assets/img/pattern-3.svg" alt="pattern" class="pattern-two d-none d-md-block">
                    <img src="<?php echo get_field('frase_de_valor')['imagen']['url']; ?>" alt="<?php echo get_field('frase_de_valor')['imagen']['alt']; ?>" class="img-fluid position-relative">
                </div>
            </div>
        </div>
    </section>

    <section class="services-section py-5 mt-5 position-relative">
        <img src="<?php bloginfo('template_url'); ?>/assets/img/pattern-2.svg" alt="pattern" class="pattern-one">
        <img src="<?php bloginfo('template_url'); ?>/assets/img/pattern-2.svg" alt="pattern" class="pattern-two">
        <div class="container">
            <div class="row">
                <?php
                    $args = array(
                        'post_type' => 'servicio',
                        'posts_per_page' => 8
                    );
                    $query = new WP_Query($args);
                ?>
                <?php if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>
                    <div class="col-6 col-lg-3 text-center icon-block mb-5">
                        <img src="<?php echo get_field('icon')['url']; ?>" alt="<?php the_title(); ?>" class="img-fluid mb-3">
                        <h3 class="h4 font-weight-light"><?php the_title(); ?></h3>
                    </div>
                <?php endwhile; endif; wp_reset_postdata(); ?>
            </div>
            <div class="text-center mt-4">
                <a href="#" class="promnite-btn quote-btn"><span>Cotiza</span></a>
            </div>
        </div>
    </section>

    <section class="py-5 latest-events position-relative">
        <img src="<?php bloginfo('template_url'); ?>/assets/img/pattern-4.svg" alt="pattern" class="pattern-one">
        <img src="<?php bloginfo('template_url'); ?>/assets/img/pattern-2.svg" alt="pattern" class="pattern-two">
        <div class="container py-5">
            <div class="row">
                <div class="col-lg-11 mx-lg-auto">
                    <h2 class="text-uppercase h6 mb-4">últimos eventos</h2>
                    <div class="font-weight-light h2">
                        <?php the_field('eventos'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="latest-events__slider">
            <?php
                $eventsArgs = array(
                    'post_type' => 'evento',
                    'posts_per_page' => -1
                );
                $eventsQuery = new WP_Query($eventsArgs);
            ?>
            <?php if($eventsQuery->have_posts()) : while($eventsQuery->have_posts()) : $eventsQuery->the_post(); ?>
                <div class="latest-events__slide px-2 px-lg-4">
                    <?php $gallery = get_field('galeria'); ?>
                    <div><img src="<?php echo $gallery[0]['url']; ?>" alt="<?php echo $gallery[0]['alt']; ?>" class="img-fluid"></div>
                </div>
            <?php endwhile; endif; wp_reset_postdata(); ?>
        </div>
        <div class="text-center mt-4">
            <a href="<?php echo get_permalink( get_page_by_path( 'eventos' ) ); ?>" class="promnite-btn"><span>Más</span></a>
        </div>
    </section>
<?php get_footer(); ?>