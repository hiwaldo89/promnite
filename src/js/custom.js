import $ from "jquery";

$(".latest-events__slider").slick({
  arrows: false,
  centerMode: true,
  slidesToShow: 1,
  centerPadding: "20%",
  autoplay: true,
  pauseOnHover: false,
  pauseOnFocus: false,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        centerPadding: "10%"
      }
    }
  ]
});

$(".promnite-header button").click(function(e) {
  e.preventDefault();
  $(".promnite-navigation").addClass("promnite-navigation--active");
});

$(".promnite-navigation button").click(function(e) {
  e.preventDefault();
  $(".promnite-navigation").removeClass("promnite-navigation--active");
});

$(".quote-btn").click(function(e) {
  e.preventDefault();
  $(".quote-form").addClass("quote-form--active");
});

$(".quote-form .close-btn").click(function(e) {
  e.preventDefault();
  $(".quote-form").removeClass("quote-form--active");
});
