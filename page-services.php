<?php
/**
 * Template Name: Services
 */
get_header(); ?>
    <section class="py-5">
        <div class="container py-5 mt-5">
            <div class="row pt-5 mt-5">
                <div class="col-lg-9 mx-lg-auto">
                    <h2 class="text-uppercase h6 mb-4">Nuestra promesa</h2>
                    <p class="h3 font-weight-light">
                        <?php the_field('promesa'); ?>
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="services-section py-5 mt-5 position-relative">
        <div class="container">
            <div class="row">
                <?php
                    $args = array(
                        'post_type' => 'servicio',
                        'posts_per_page' => -1
                    );
                    $query = new WP_Query($args);
                ?>
                <?php if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>
                    <div class="col-6 col-lg-3 text-center icon-block mb-5">
                        <img src="<?php echo get_field('icon')['url']; ?>" alt="<?php the_title(); ?>" class="img-fluid mb-3">
                        <h3 class="h4 font-weight-light"><?php the_title(); ?></h3>
                    </div>
                <?php endwhile; endif; wp_reset_postdata(); ?>
            </div>
            <div class="row my-5 py-5">
                <div class="col-lg-9 mx-lg-auto">
                    <p class="h3 font-weight-light">
                        <?php the_field('cierre'); ?>
                    </p>
                </div>
            </div>
            <div class="text-center mt-4">
                <a href="#" class="promnite-btn quote-btn"><span>Cotiza</span></a>
            </div>
        </div>
    </section>

    <div class="grid-gallery d-lg-flex justify-content-between pb-5 mb-5">
        <div class="grid-gallery__col">
            <div class="grid-gallery__item grid-gallery__item--tall" style="background-image: url(<?php echo get_field('galeria')[0]['url']; ?>);"></div>
        </div>
        <div class="grid-gallery__col d-flex flex-column">
            <div class="grid-gallery__item grid-gallery__item--square" style="background-image: url(<?php echo get_field('galeria')[1]['url']; ?>);"></div>
            <div class="grid-gallery__item grid-gallery__item--square mt-auto" style="background-image: url(<?php echo get_field('galeria')[2]['url']; ?>);"></div>
        </div>
    </div>
<?php get_footer(); ?>