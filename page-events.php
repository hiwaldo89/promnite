<?php
/**
 * Template Name: Events
 */
get_header(); ?>
    <section class="py-5 my-5">
        <div class="container py-5 mt-5">
            <div class="row">
                <?php
                    $eventsArgs = array(
                        'post_type' => 'evento',
                        'posts_per_page' => -1
                    );
                    $eventsQuery = new WP_Query($eventsArgs);
                ?>
                <?php if($eventsQuery->have_posts()) : while($eventsQuery->have_posts()) : $eventsQuery->the_post(); ?>
                    <div class="col-lg-6 mb-4">
                        <div class="event-block">
                            <img src="<?php echo get_field('galeria')[0]['url']; ?>" alt="<?php echo get_field('galeria')[0]['alt']; ?>" class="img-fluid">
                            <div class="event-block__info d-flex align-items-center justify-content-center flex-column">
                                <h3 class="font-italic"><?php the_title(); ?></h3>
                                <p class="font-weight-light h5 letter-spacing"><?php the_field('subtitulo'); ?></p>
                            </div>
                        </div>
                    </div>
                <?php endwhile; endif; wp_reset_postdata(); ?>
            </div>
            <div class="text-center mt-5">
                <a href="" class="promnite-btn"><span>Más</span></a>
            </div>
        </div>
    </section>

    <section class="py-5">
        <div class="container text-center pb-5 mb-5">
            <div class="row">
                <div class="col-lg-6 mx-lg-auto">
                    <div class="h2 font-italic mb-5"><?php the_content(); ?></div>
                </div>
            </div>
            <a href="" class="promnite-btn quote-btn"><span>Cotiza</span></a>
        </div>
    </section>
<?php get_footer(); ?>