<?php

function promnite_setup() {
    load_theme_textdomain( 'promnite', get_template_directory() . '/languages' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ) );
}
add_action( 'after_setup_theme', 'promnite_setup' );

function promnite_scripts() {
    wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Dosis|Raleway:300,700i&display=swap' );
    wp_enqueue_style( 'promnite-style', get_stylesheet_uri() );

    wp_enqueue_script( 'promnite-js', get_template_directory_uri() . '/assets/js/app.js', '', '', true );
}
add_action('wp_enqueue_scripts', 'promnite_scripts');

// GravityForms compatibility file
require_once get_template_directory() . '/inc/promnite-gravityforms.php';