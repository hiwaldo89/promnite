<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php gravity_form_enqueue_scripts( 1 ); ?>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <header class="promnite-header py-5">
        <div class="container">
            <div class="d-flex">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="promnite-header__logo mr-auto">
                    <img src="<?php bloginfo('template_url'); ?>/assets/img/logo.svg" alt="Promnite">
                </a>
                <div>
                    <button class="promnite-btn"><span>Menú</span></button>
                </div>
            </div>
        </div>
    </header>

    <nav class="promnite-navigation d-flex align-items-center justify-content-center">
        <div>
            <h2 class="h4 text-uppercase mb-5 position-relative">
                Promnite
                <button class="d-flex align-items-center justify-content-center">
                    <img src="<?php bloginfo('template_url'); ?>/assets/img/close-icon.svg" alt="Cerrar menú">
                </button>
            </h2>
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="text-uppercase h1 font-italic d-block">Inicio</a>
            <a href="<?php echo get_permalink( get_page_by_path('servicios') ); ?>" class="text-uppercase h1 font-italic d-block">Servicios</a>
            <a href="<?php echo get_permalink( get_page_by_path('venues') ); ?>" class="text-uppercase h1 font-italic d-block">Venues</a>
            <a href="<?php echo get_permalink( get_page_by_path('eventos') ); ?>" class="text-uppercase h1 font-italic d-block">Eventos</a>
            <!-- <a href="" class="text-uppercase h1 font-italic d-block">Cotiza</a> -->
            <a href="http://app.promnite.com.mx/login" class="d-flex align-items-center mt-5 pt-4">
                <img src="<?php bloginfo('template_url'); ?>/assets/img/user.svg" alt="iniciar sesión" width="25" class="mr-3">
                <span class="text-uppercase h2 font-italic mb-0">Login</span>
            </a>
            <a href="mailto:info@promnite.com.mx" class="font-weight-light h5 d-block">info@promnite.com.mx</a>
            <a href="tel:4424444444" class="font-weight-light h5 d-block">442444 4444</a>
        </div>
    </nav>

    <div class="quote-form d-flex align-items-center justify-content-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 mx-lg-auto">
                    <h3 class="font-italic text-lg-center mb-4 position-relative">
                        Let's party all night!
                        <button class="d-flex align-items-center justify-content-center close-btn">
                            <img src="<?php bloginfo('template_url'); ?>/assets/img/close-icon.svg" alt="Cerrar menú">
                        </button>
                    </h3>
                    <?php gravity_form(1, false, false, false, false, true); ?>
                </div>
            </div>
        </div>
    </div>