<?php
/**
 * Template Name: Venues
 */
get_header(); ?>
    <section class="py-5 my-5">
        <div class="container py-5">
            <?php 
                $venueArgs = array(
                    'post_type' => 'venue',
                    'posts_per_page' => -1
                );
                $venueQuery = new WP_Query($venueArgs);
            ?>
            <?php if($venueQuery->have_posts()) : while($venueQuery->have_posts()) : $venueQuery->the_post(); ?>
                <div class="row p-lg-5 venue-block">
                    <div class="col-lg-6">
                        <img src="<?php echo get_field('galeria')[0]['url']; ?>" alt="<?php echo get_field('galeria')[0]['alt']; ?>" class="img-fluid">
                    </div>
                    <div class="col-lg-6">
                        <p class="h4 font-weight-light mb-0 py-4"><?php the_title(); ?></p>
                        <p class="h4 font-weight-light mb-0 py-4"><?php the_field('ubicacion'); ?></p>
                        <p class="h4 font-weight-light mb-0 py-4"><?php the_field('pax'); ?></p>
                        <p class="h4 font-weight-light mb-0 py-4"><?php the_field('ideal_para'); ?></p>
                    </div>
                </div>
            <?php endwhile; endif; wp_reset_postdata(); ?>
            <div class="text-center mt-5">
                <a href="#" class="promnite-btn quote-btn"><span>Cotiza</span></a>
            </div>
        </div>
    </section>
<?php get_footer(); ?>