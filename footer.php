        <footer class="promnite-footer py-5 position-relative">
            <img src="<?php bloginfo('template_url'); ?>/assets/img/pattern-2.svg" alt="pattern" class="pattern-one">
            <img src="<?php bloginfo('template_url'); ?>/assets/img/pattern-3.svg" alt="pattern" class="pattern-two">
            <div class="container py-5 text-center position-relative">
                <img src="<?php bloginfo('template_url'); ?>/assets/img/logo.svg" alt="Promnite" class="footer-logo mb-4">
                <p class="h3 font-italic mb-5">
                    Let’s party all night!
                </p>
                <a href="mailto:info@promnite.com.mx" class="font-weight-light h5 d-block">info@promnite.com.mx</a>
                <a class="font-weight-light h5 d-block" href="tel:4424444444">442444 4444</a>
            </div>
        </footer>
        <?php wp_footer(); ?>
    </body>
</html>